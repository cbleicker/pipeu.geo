<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Utility;

use TYPO3\Flow\Reflection\ObjectAccess;

/**
 * Class Closures
 *
 * @package PIPEU\Geo\Utility
 */
class Closures {

	const SORT_DESC = -1, SORT_ASC = 1;

	/**
	 * @param string $propertyName
	 * @param integer $orientation
	 * @return \Closure
	 */
	public static function sortByField($propertyName, $orientation = Closures::SORT_ASC) {
		return function ($a, $b) use ($propertyName, $orientation) {
			$aValue = self::getProperty($a, $propertyName);
			$bValue = self::getProperty($b, $propertyName);
			return (($aValue > $bValue) ? 1 : -1) * $orientation;
		};
	}

	/**
	 * @param object|array $subject
	 * @param string|integer $propertyPath
	 * @return mixed
	 * @throws \InvalidArgumentException
	 */
	protected static function getProperty($subject, $propertyPath) {
		if (!is_object($subject) && !is_array($subject)) {
			throw new \InvalidArgumentException('$subject must be an object or array, ' . gettype($subject) . ' given.', 1397667990);
		}
		if (!is_string($propertyPath) && !is_integer($propertyPath)) {
			throw new \InvalidArgumentException('Given propertyPath is not of type string or integer.', 1397667991);
		}
		return ObjectAccess::getPropertyPath($subject, $propertyPath);
	}
}
