<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Domain\Model\Interfaces;

use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;

/**
 * Class InterfacePostal
 *
 * @package PIPEU\Geo\Domain\Model\Interfaces
 */
interface InterfacePostal {

	/**
	 * @param string $address
	 *
	 * @return $this
	 */
	public function setAddress($address);

	/**
	 * @return string
	 */
	public function getAddress();

	/**
	 * @param AbstractCountry $country
	 *
	 * @return $this
	 */
	public function setCountry(AbstractCountry $country = NULL);

	/**
	 * @return AbstractCountry
	 */
	public function getCountry();

	/**
	 * @param string $city
	 *
	 * @return $this
	 */
	public function setCity($city);

	/**
	 * @return string
	 */
	public function getCity();

	/**
	 * @param string $code
	 *
	 * @return $this
	 */
	public function setCode($code);

	/**
	 * @return string
	 */
	public function getCode();

	/**
	 * @param string $county
	 *
	 * @return $this
	 */
	public function setCounty($county);

	/**
	 * @return string
	 */
	public function getCounty();
}
