<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Domain\Model\Traits;

/**
 * Class TraitCountry
 *
 * @package PIPEU\Geo\Domain\Model\Traits
 */
trait TraitCountry {

	/**
	 * @return string
	 */
	public function getIso2() {
		return $this->iso2;
	}

	/**
	 * @return string
	 */
	public function getIso3() {
		return $this->iso3;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * @return boolean
	 */
	public function getEuMember() {
		return $this->euMember;
	}
}
