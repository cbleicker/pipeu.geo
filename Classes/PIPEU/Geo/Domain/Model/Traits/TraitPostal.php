<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Domain\Model\Traits;

use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;

/**
 * Class TraitPostal
 *
 * @package PIPEU\Geo\Domain\Model\Traits
 */
trait TraitPostal {

	/**
	 * @param string $address
	 *
	 * @return $this
	 */
	public function setAddress($address) {
		$this->address = $address;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * @param AbstractCountry $country
	 *
	 * @return $this
	 */
	public function setCountry(AbstractCountry $country = NULL) {
		$this->country = $country;
		return $this;
	}

	/**
	 * @return AbstractCountry
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * @param string $city
	 *
	 * @return $this
	 */
	public function setCity($city) {
		$this->city = $city;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * @param string $code
	 *
	 * @return $this
	 */
	public function setCode($code) {
		$this->code = $code;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCode() {
		return $this->code;
	}

	/**
	 * @param string $county
	 *
	 * @return $this
	 */
	public function setCounty($county) {
		$this->county = $county;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCounty() {
		return $this->county;
	}
}
