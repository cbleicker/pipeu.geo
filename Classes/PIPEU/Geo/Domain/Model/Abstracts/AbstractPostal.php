<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Domain\Model\Abstracts;

use PIPEU\Geo\Domain\Model\Interfaces\InterfacePostal;
use PIPEU\Geo\Domain\Model\Traits\TraitPostal;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractPostal
 *
 * @package PIPEU\Geo\Domain\Model\Abstracts
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractPostal implements InterfacePostal {

	use TraitPostal;

	/**
	 * @var string
	 * @ORM\Column(type="text")
	 */
	protected $address;

	/**
	 * @var string
	 */
	protected $city;

	/**
	 * @var string
	 */
	protected $code;

	/**
	 * @var string
	 * @ORM\Column(nullable=true)
	 */
	protected $county;

	/**
	 * @var AbstractCountry
	 * @ORM\ManyToOne
	 * @ORM\JoinColumn(onDelete="SET NULL")
	 * @ORM\Column(nullable=true)
	 */
	protected $country;
}
