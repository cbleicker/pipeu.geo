<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Domain\Model\Abstracts;

use PIPEU\Geo\Domain\Model\Interfaces\InterfaceCountry;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class AbstractCountry
 *
 * @package PIPEU\Geo\Domain\Model\Abstracts
 * @Flow\Entity
 * @ORM\InheritanceType("JOINED")
 */
abstract class AbstractCountry implements InterfaceCountry {

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 * @Flow\Identity
	 * @ORM\Id
	 * @ORM\Column(length=2)
	 */
	protected $iso2;

	/**
	 * @var string
	 */
	protected $iso3;

	/**
	 * @var integer
	 */
	protected $number;

	/**
	 * @var boolean
	 */
	protected $euMember;

	/**
	 * @param string $iso2
	 * @param string $iso3
	 * @param string $name
	 * @param integer $number
	 * @param boolean $euMember
	 */
	public function __construct($iso2, $iso3, $name, $number, $euMember = FALSE) {
		$this->iso2 = $iso2;
		$this->iso3 = $iso3;
		$this->name = $name;
		$this->number = $number;
		$this->euMember = $euMember;
	}

	/**
	 * @return string
	 */
	public function getIso2() {
		return $this->iso2;
	}

	/**
	 * @return string
	 */
	public function getIso3() {
		return $this->iso3;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * @return boolean
	 */
	public function getEuMember() {
		return $this->euMember;
	}
}
