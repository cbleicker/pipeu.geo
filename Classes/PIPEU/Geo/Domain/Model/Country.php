<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\Domain\Model;

use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;
use TYPO3\Flow\Annotations as Flow;

/**
 * Class Country
 *
 * @package PIPEU\Geo\Domain\Model
 * @Flow\Entity
 */
class Country extends AbstractCountry {

}
