<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\ViewHelpers;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Reflection\ObjectAccess;
use TYPO3\Fluid\Core\ViewHelper;
use TYPO3\Fluid\ViewHelpers\TranslateViewHelper;
use Doctrine\Common\Collections\ArrayCollection;
use PIPEU\Geo\Domain\Model\Abstracts\AbstractCountry;
use TYPO3\Flow\Persistence\Doctrine\QueryResult;


/**
 * Class SortViewHelper
 *
 * @package PIPEU\Geo\ViewHelpers
 */
class TranslateCountriesViewHelper extends TranslateViewHelper {

	/**
	 * @param string $id
	 * @param QueryResult $value
	 * @param array $arguments
	 * @param string $source
	 * @param string $package
	 * @param mixed $quantity
	 * @param string $locale
	 * @return ArrayCollection|NULL $value
	 * @throws ViewHelper\Exception
	 */
	public function render($id = NULL, $value = NULL, array $arguments = array(), $source = 'Main', $package = NULL, $quantity = NULL, $locale = NULL) {

		/** @var QueryResult $value */
		$value = !$value instanceof QueryResult ? $this->renderChildren() : $value;

		if (!$value instanceof QueryResult) {
			return NULL;
		}

		$value = new ArrayCollection($value->toArray());

		while ($value->current()) {
			/** @var AbstractCountry $country */
			$country        = $value->current();
			$translatedName = parent::render($country->getIso2(), $country->getName(), $arguments, $source, $package, $quantity, $locale);
			ObjectAccess::setProperty($country, 'name', $translatedName, TRUE);
			$value->next();
		}

		return $value;
	}
}

?>
