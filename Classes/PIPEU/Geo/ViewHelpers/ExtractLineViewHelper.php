<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\ViewHelpers;

use TYPO3\Flow\Utility\Arrays;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * Class ExtractLineViewHelper
 *
 * @package PIPEU\Geo\ViewHelpers
 */
class ExtractLineViewHelper extends AbstractViewHelper {

	/**
	 * @param string $value
	 * @param integer $line
	 *
	 * @return string
	 */
	public function render($value, $line = 0) {
		$lines = Arrays::trimExplode("\n", $value);
		return Arrays::getValueByPath($lines, $line);
	}
}
