<?php
/*                                                                        *
 * This script belongs to the TYPO3 Flow framework.                       *
 *                                                                        *
 * It is free software; you can redistribute it and/or modify it under    *
 * the terms of the GNU Lesser General Public License, either version 3   *
 * of the License, or (at your option) any later version.                 *
 *                                                                        *
 * The TYPO3 project - inspiring people to share!                         *
 *                                                                        */

namespace PIPEU\Geo\ViewHelpers;

use Doctrine\Common\Collections\Collection;
use PIPEU\Geo\Utility\Closures;
use TYPO3\Flow\Exception;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;
use ArrayObject;

/**
 * Class SortViewHelper
 *
 * @package PIPEU\Geo\ViewHelpers
 */
class SortViewHelper extends AbstractViewHelper {

	/**
	 * @var boolean
	 */
	protected $escapingInterceptorEnabled = FALSE;

	/**
	 * @param array|ArrayObject|Collection $subject
	 * @param string $propertyName
	 * @param integer $orientation
	 * @return array
	 * @throws \InvalidArgumentException
	 */
	public function render($subject = NULL, $propertyName = NULL, $orientation = Closures::SORT_ASC) {

		$subject = $subject === NULL ? $this->renderChildren() : $subject;

		if($propertyName === NULL || strlen($propertyName) === 0){
			throw new \InvalidArgumentException('$propertyName has to be a valid string.', 1397667999);
		}

		if ($subject instanceof ArrayObject || $subject instanceof Collection || is_array($subject)) {

			if ($subject instanceof ArrayObject) {
				$subject = $subject->getArrayCopy();
			}

			if ($subject instanceof Collection) {
				$subject = $subject->toArray();
			}

			$closure  = Closures::sortByField($propertyName, $orientation);
			$hasError = !usort($subject, $closure);

			if ($hasError === TRUE) {
				throw new Exception('Sorting failed. Sorry for less information. But you´re using php ;). phps usort() just return true/false for sorting errors :(', 1397667989);
			}
		} else {
			throw new \InvalidArgumentException('$subject must be an array || ArrayObject || \Doctrine\Common\Collections\Collection, ' . gettype($subject) . ' given.', 1397667989);
		}

		return $subject;
	}
}

?>
